package vpcserver

/**
 *  通信协议----设计
 *  标识协议   预留字段   对象二进制转换    256个
 *  魔数 ----- 版本号 -- 序列化算法 ------- 指令 --数据长度 -- 数据
 *  4Byte      1Byte       1Byte           1Byte    4Byte     4Byte
 */
var (
	MagicNumber      = 0x12345678   //魔数
	Version          = []byte{0x01} //协议版本
	JsonSerializer   = []byte{0x01} //序列化版本
	VpcAgentRequest  = []byte{0x61} //VpcAgent注册指令
	VpcAgentResponse = []byte{0x62} //VpcAgent注册指令
)

// PayLoadEncode /* 命令报文编码封装 */
func PayLoadEncode(cmd, head, body []byte) []byte {
	header := make([]byte, 11)
	copy(header[0:4], IntToBytes(MagicNumber))
	copy(header[4:5], Version)
	copy(header[5:6], JsonSerializer)
	copy(header[6:7], cmd)
	copy(header[7:11], IntToBytes(len(head)))
	header = append(header, head...)
	if body == nil {
		body = []byte{}
	}
	bodyHeader := make([]byte, 4)
	copy(bodyHeader[0:4], IntToBytes(len(body)))
	bodyHeader = append(bodyHeader, body...)
	return append(header, bodyHeader...)
}

// PayLoad /* 报文载荷 */
type PayLoad struct {
	magic      int    //头部长度
	version    []byte //协议版本号
	serializer []byte //序列化方式
	command    []byte //指令类型代号
	headLen    int    //header数据体长度
	head       []byte //头部数据体
	bodyLen    int    //body数据体长度
	body       []byte //Body数据体
}
